import 'dart:convert';
import 'package:http/http.dart' as http;

const String kApiURL = 'https://rest.coinapi.io/v1/exchangerate';
const String kApiKey = '67D80B00-7E67-4C00-9621-C3D32D22791A';

const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

class CoinData {
  Future<List<String>> getCoinData(String currency) async {
    List<String> rates = [];
    double rate;
    for (String crypto in cryptoList) {
      final String url = '$kApiURL/$crypto/$currency?apikey=$kApiKey';
      var response = await http.get(url);
      if (response.statusCode == 200) {
        rate = jsonDecode(response.body)['rate'];
      } else {
        rate = 0.00;
        print('Request failed with status: ${response.statusCode}.');
      }
      rates.add(rate.toStringAsFixed(2));
    }
    return rates;
  }
}
